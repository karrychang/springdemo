package com.envision.demo2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <strong>Title : PrePostConfig.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月08日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Configuration
@ComponentScan("com.envision.demo2")
public class PrePostConfig {
    @Bean(initMethod = "init" ,destroyMethod = "destroy")
    BeanWayService beanWayService(){
        return new BeanWayService();
    }

}
