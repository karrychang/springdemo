package com.envision.demo2;

/**
 * <strong>Title : BeanWayService.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月08日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class BeanWayService {

    public void init(){
        System.out.println("@bean-init-method");
    }

    public BeanWayService() {
        System.out.println("初始化构造函数-beanwayservice");
    }
    public void destroy(){
        System.out.println("@bean-destory-method");
    }
}
