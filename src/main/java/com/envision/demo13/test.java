package com.envision.demo13;

/**
 * <strong>Title : test.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月23日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class test {
    public static void main(String []args)
    {
        int i,sum;
        sum=0;

        for(i=1;i<=10;i++)
            sum+=i;
        System.out.println("sum="+sum);
    }
}
