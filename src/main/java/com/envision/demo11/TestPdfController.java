package com.envision.demo11;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * <strong>Title : PdfController.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月13日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@RestController
public class TestPdfController {

    @RequestMapping("/exportpdf2")
    public String exportpdf(HttpServletRequest request ,HttpServletResponse response) throws UnsupportedEncodingException {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        String filename="中山广场报告.pdf";
        String path="d:/";
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment;fileName="
                + URLEncoder.encode(filename, "UTF-8"));
        OutputStream os = null;
        PdfStamper ps = null;
        PdfReader reader = null;
        try {
            os = response.getOutputStream();
            // 2 读入pdf表单
            reader = new PdfReader(new URL(request.getRequestURL().toString().replace("exportpdf1","page/report.pdf")));
            // 3 根据表单生成一个新的pdf
            ps = new PdfStamper(reader, os);
            // 4 获取pdf表单
            AcroFields form = ps.getAcroFields();
            // 5给表单添加中文字体 这里采用系统字体。不设置的话，中文可能无法显示
            BaseFont bf = BaseFont.createFont("C:/WINDOWS/Fonts/SIMSUN.TTC,1",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            form.addSubstitutionFont(bf);
            // 6查询数据================================================
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("name", "SOHO中山广场");
            data.put("projectname", "上海SOHO中山广场");
            data.put("address", "上海市长宁区中山西路1065号");
            data.put("cap", "800kWh");
            data.put("power", "400kW");
            data.put("power1", "400");
            data.put("num", "8");
            data.put("cap1", "test");
            // 7遍历data 给pdf表单表格赋值
            for (String key : data.keySet()) {
                form.setField(key,data.get(key).toString());
            }
            ps.setFormFlattening(true);
            //-----------------------------pdf 添加图片----------------------------------
            // 通过域名获取所在页和坐标，左下角为起点
//            System.out.println("pdf 添加图片");
//            String imgpath="d:/美女.png";


            // 读图片
            String url=request.getRequestURL().toString().replace(request.getRequestURI(),"/");
            String[] strs={"imageline","imagebar","imagelinetest"};
            int i=1;
            for (String string:strs){
                int pageNo = form.getFieldPositions("image"+i).get(0).page;
                Rectangle signRect = form.getFieldPositions("image"+i).get(0).position;
                float x = signRect.getLeft();
                float y = signRect.getBottom();
                Image image = Image.getInstance(new URL(url+"/"+string));
//            Image image = Image.getInstance(imgpath);
                // 获取操作的页面
                PdfContentByte under = ps.getOverContent(pageNo);
                // 根据域的大小缩放图片
                image.scaleToFit(signRect.getWidth(), signRect.getHeight());
                // 添加图片
                image.setAbsolutePosition(x, y);
                under.addImage(image);
                i++;
            }

            //-------------------------------------------------------------
            System.out.println("===============PDF   success=============");
        } catch (Exception e) {
            System.out.println("===============PDF failed=============");
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                reader.close();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }



}
