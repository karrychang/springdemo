package com.envision.demo11;

import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleInsets;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.IOException;

/**
 * <strong>Title : JFreeChartUtils.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月13日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@RestController
public class JFreeChartUtils {


//    @RequestMapping("/imagePie")
//    public void createimage(HttpServletResponse response) throws IOException {
//        response.setContentType("image/jpeg");
//        DefaultPieDataset data = getDataSet();
//        JFreeChart chart = ChartFactory.createPieChart3D("水果产量图",
//                data,
//                true,
//                false,
//                false
//        );
//        ChartUtilities.writeChartAsJPEG(response.getOutputStream(),
//                1,chart,400,300,null);
//    }

    private static DefaultPieDataset getDataSet() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("苹果",100);
        dataset.setValue("梨子",200);
        dataset.setValue("葡萄",300);
        dataset.setValue("香蕉",400);
        dataset.setValue("荔枝",500);
        return dataset;
    }

    @RequestMapping("/imagebar1")
    public void createimage1(HttpServletRequest request,HttpServletResponse response) throws IOException {
        CategoryDataset dataset = getDataSet2();
         /**解决乱码**/
        //创建主题样式
        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
        // standardChartTheme.setLargeFont(new Font("黑体",Font.BOLD,20));
        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN,14));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋体",Font.PLAIN,9));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋体",Font.PLAIN,9));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
        JFreeChart chart = ChartFactory.createBarChart("电价分布", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );


//        chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
//        chart.setBackgroundPaint(ChartColor.white);
//        chart.setTextAntiAlias(false);
        chart.setBorderVisible(false);//边界不可见
        chart.getLegend().setBorder(0,0,0,0);//图示边框

        Plot plot=chart.getPlot();
        plot.setBackgroundPaint(ChartColor.white);//背景白色
//        plot.setOutlinePaint(ChartColor.white);//数据区边界白色
        plot.setOutlineVisible(false);

        CategoryPlot categoryPlot=chart.getCategoryPlot();
//        categoryPlot.setDomainGridlinesVisible(false);
        categoryPlot.setRangeGridlinePaint(Color.lightGray);//横向线颜色
        categoryPlot.setRangeGridlinesVisible(true);//是否可见
        categoryPlot.setRangeGridlineStroke(new BasicStroke(0.5f));//横向背景线为实线
        categoryPlot.setAxisOffset(new RectangleInsets(1D, 0D, 0D, 2D));


        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);//横坐标偏移
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));//设置X轴坐标上的文字
        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));//设置X轴的标题文字
//        domainAxis.setLabel("");//X轴的标题内容
//        domainAxis.setTickLabelPaint(Color.red);//X轴的标题文字颜色
//        domainAxis.setTickLabelsVisible(true);//X轴的标题文字是否显示
//        domainAxis.setAxisLinePaint(Color.red);//X轴横线颜色
        domainAxis.setTickMarksVisible(false);//标记线是否显示
//        domainAxis.setTickMarkOutsideLength(3);//标记线向外长度
//        domainAxis.setTickMarkInsideLength(3);//标记线向内长度
//        domainAxis.setTickMarkPaint(Color.red);//标记线颜色
        domainAxis.setUpperMargin(0.005);//设置距离图片左端距离
        domainAxis.setLowerMargin(0.005); //设置距离图片右端距离
     //横轴上的 Lable 是否完整显示
//        domainAxis.setMaximumCategoryLabelWidthRatio(0.6f);
        //横轴上的 Lable 45度倾斜
//        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);






        ValueAxis rAxis =categoryPlot.getRangeAxis();//对Y轴做操作
        //设置Y轴坐标上的文字
        rAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));
        //设置Y轴的标题文字
//        rAxis.setLabelFont(new Font("黑体", Font.PLAIN, 12));
        //Y轴取值范围（下面不能出现 rAxis.setAutoRange(true) 否则不起作用）
//        rAxis.setRange(100, 600);
        // rAxis.setLowerBound(100); //Y轴以开始的最小值
        // rAxis.setUpperBound(600);//Y轴的最大值
//        rAxis.setLabel("content");//Y轴内容
//        rAxis.setLabelAngle(1.6);//标题内容显示角度（1.6时候水平）
//        rAxis.setLabelPaint(Color.red);//标题内容颜色
//        rAxis.setMinorTickMarksVisible(true);//标记线是否显示
//        rAxis.setMinorTickCount(7);//节段中的刻度数
//        rAxis.setMinorTickMarkInsideLength(3);//内刻度线向内长度
//        rAxis.setMinorTickMarkOutsideLength(3);//内刻度记线向外长度
//        rAxis.setTickMarkInsideLength(3);//外刻度线向内长度
        rAxis.setTickMarkPaint(Color.white);//刻度线颜色
//        rAxis.setTickMarksVisible(false);
//        rAxis.setAutoRangeMinimumSize(0.2);
        rAxis.setTickLabelsVisible(true);//刻度数值是否显示
        // 所有Y标记线是否显示（如果前面设置rAxis.setMinorTickMarksVisible(true); 则其照样显示）
        rAxis.setTickMarksVisible(true);
//       rAxis.setAxisLinePaint(Color.red);//Y轴竖线颜色
        rAxis.setAxisLineVisible(false);//Y轴竖线是否显示
//设置最高的一个 Item 与图片顶端的距离 (在设置rAxis.setRange(100, 600);情况下不起作用)
//        rAxis.setUpperMargin(0.15);
//设置最低的一个 Item 与图片底端的距离
//        rAxis.setLowerMargin(0);
//        rAxis.setAutoRange(true);//是否自动适应范围
        rAxis.setVisible(true);//Y轴内容是否显示
        NumberAxis numberAxis = (NumberAxis)rAxis;
        numberAxis .setAutoTickUnitSelection(false);
        double unit=0.2;//刻度的长度
        NumberTickUnit ntu= new NumberTickUnit(unit);
        numberAxis .setTickUnit(ntu);


        BarRenderer renderer=new BarRenderer();
        renderer.setItemMargin(0.001);//设置每个地区所包含的平行柱的之间距离
        renderer.setDrawBarOutline(false);//边框
        renderer.setShadowVisible(false);//阴影
        renderer.setSeriesPaint(0, new Color(68, 92, 187));//颜色设置
        renderer.setSeriesPaint(1, new Color(187, 68, 68));
        renderer.setBarPainter(new StandardBarPainter());//取消柱子渐变效果
        renderer.setMaximumBarWidth(0.01);//柱子宽度百分比
        categoryPlot.setRenderer(renderer);


        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 900, 520,
                null);

    }

//    private static CategoryDataset getDataSetbar() {
//        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//        dataset.addValue(100, "", "苹果");
//        dataset.addValue(200, "", "梨子");
//        dataset.addValue(300, "", "葡萄");
//        dataset.addValue(400, "", "香蕉");
//        dataset.addValue(500, "", "荔枝");
//        return dataset;
//    }
    private static CategoryDataset getDataSet2() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<24;i++){
            if(i==23){
                dataset.addValue(Math.random(), "夏季", "23~0");
                dataset.addValue(Math.random(), "非夏季", "23~0");
                break;
            }
            dataset.addValue(Math.random(), "夏季", i+"~"+(i+1));
            dataset.addValue(Math.random(), "非夏季", i+"~"+(i+1));
        }


        return dataset;
    }
//    @RequestMapping("/imagearea")
//    public void createarea(HttpServletResponse response) throws IOException {
//        CategoryDataset dataset = getDataSet2();
//
//        //创建主题样式
//        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
//        //设置标题字体
//        // standardChartTheme.setLargeFont(new Font("黑体",Font.BOLD,20));
//        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN,14));
//        //设置图例的字体
//        standardChartTheme.setRegularFont(new Font("宋体",Font.PLAIN,9));
//        //设置轴向的字体
//        standardChartTheme.setLargeFont(new Font("宋体",Font.PLAIN,9));
//        //应用主题样式
//        ChartFactory.setChartTheme(standardChartTheme);
//        JFreeChart chart =ChartFactory.createAreaChart("水果产量图", // 图表标题
//                "水果", // 目录轴的显示标签
//                null, // 数值轴的显示标签
//                dataset, // 数据集
//                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
//                true, // 是否显示图例(对于简单的柱状图必须是 false)
//                false, // 是否生成工具
//                false // 是否生成 URL 链接
//                 );
//
//        chart.setBorderVisible(false);//边界不可见
//        chart.getLegend().setBorder(0,0,0,0);//图示边框
//
//        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1f, chart, 600, 400,
//                null);
//    }

    @RequestMapping("/imageline1")
    public void createarea1(HttpServletResponse response) throws IOException {
        CategoryDataset dataset = createDataset();
        //创建主题样式
        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
        // standardChartTheme.setLargeFont(new Font("黑体",Font.BOLD,20));
        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN,14));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋体",Font.PLAIN,9));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋体",Font.PLAIN,9));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
        JFreeChart chart =ChartFactory.createLineChart("收益总览单位：元", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );

        jfreechartconfig(chart);
        CategoryPlot categoryPlot=chart.getCategoryPlot();
        LineAndShapeRenderer renderer=(LineAndShapeRenderer)categoryPlot.getRenderer();
        renderer.setSeriesStroke(0, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(0, new Color(34,146,211));//红色
        renderer.setSeriesStroke(1, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(1, new Color(187,68,68));//红色
        renderer.setSeriesStroke(2, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(2,new Color(138,179,77));//红色

//        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 600, 400,
//                null);
        ChartUtilities.writeChartAsPNG(response.getOutputStream(), chart, 900, 520, true,
                100);
    }

    public static CategoryDataset createDataset() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=1;i<11;i++){
            if(i==23){
                dataset.addValue(Math.random(), "夏季", "第"+i+"年");
                dataset.addValue(Math.random(), "非夏季", "23~0");
                break;
            }
            dataset.addValue(Math.random()*1000000, "方案一", "第"+i+"年");
            dataset.addValue(Math.random()*1000000, "方案二", "第"+i+"年");
            dataset.addValue(Math.random()*1000000, "方案三", "第"+i+"年");
        }
        return dataset;
    }

    public void jfreechartconfig(JFreeChart chart){
        chart.setBorderVisible(false);//边界不可见
        chart.getLegend().setBorder(0,0,0,0);//图示边框

        Plot plot=chart.getPlot();
        plot.setBackgroundPaint(ChartColor.white);//背景白色
        plot.setOutlineVisible(false);

        CategoryPlot categoryPlot=chart.getCategoryPlot();
        categoryPlot.setRangeGridlinePaint(Color.lightGray);//横向线颜色
        categoryPlot.setRangeGridlinesVisible(true);//是否可见
        categoryPlot.setRangeGridlineStroke(new BasicStroke(0.5f));//横向背景线为实线
        categoryPlot.setAxisOffset(new RectangleInsets(1D, 0D, 0D, 2D));


        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));//设置X轴坐标上的文字
        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));//设置X轴的标题文字
        domainAxis.setTickMarksVisible(false);//标记线是否显示
        domainAxis.setUpperMargin(0.005);//设置距离图片左端距离
        domainAxis.setLowerMargin(0.005); //设置距离图片右端距离



        ValueAxis rAxis =categoryPlot.getRangeAxis();//对Y轴做操作
        //设置Y轴坐标上的文字
        rAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));
        rAxis.setTickMarkPaint(Color.white);//刻度线颜色
        rAxis.setTickLabelsVisible(true);//刻度数值是否显示
        rAxis.setTickMarksVisible(true);
        rAxis.setAxisLineVisible(false);//Y轴竖线是否显示

    }

    @RequestMapping("/imagelinetest1")
    public void createarea2(HttpServletResponse response) throws IOException {
        CategoryDataset dataset = createDataset1();
        //创建主题样式
        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
        // standardChartTheme.setLargeFont(new Font("黑体",Font.BOLD,20));
        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN,14));
        //设置图例的字体
        standardChartTheme.setRegularFont(new Font("宋体",Font.PLAIN,9));
        //设置轴向的字体
        standardChartTheme.setLargeFont(new Font("宋体",Font.PLAIN,9));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
        JFreeChart chart =ChartFactory.createLineChart("负荷分析", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );

        jfreechartconfig(chart);
        CategoryPlot categoryPlot=chart.getCategoryPlot();
        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);//横坐标偏移

        LineAndShapeRenderer renderer=(LineAndShapeRenderer)categoryPlot.getRenderer();
        renderer.setBaseShapesVisible(true);
        renderer.setBaseShapesFilled(true);
        renderer.setBaseFillPaint(Color.blue);
        renderer.setSeriesStroke(0, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(0, Color.blue);//红色
        renderer.setSeriesShape(0, new Ellipse2D.Double(-2D, -2D, 4D, 4D));


        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 900, 520, null);
    }
    public static CategoryDataset createDataset1() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<24;i++){
            if(i==23){
                dataset.addValue(Math.random()*50000, "负荷分析", "23~0");
                break;
            }
            dataset.addValue(Math.random()*50000, "负荷分析", i+"~"+(i+1));
        }
        return dataset;
    }

}
