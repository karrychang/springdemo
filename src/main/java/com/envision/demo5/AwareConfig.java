package com.envision.demo5;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <strong>Title : AwareConfig.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月11日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Configuration
@ComponentScan("com.envision.demo5")
public class AwareConfig {
}
