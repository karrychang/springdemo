package com.envision.demo5;

import org.codehaus.plexus.util.IOUtil;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;


/**
 * <strong>Title : AwareService.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月11日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Service
public class AwareService implements BeanNameAware,ResourceLoaderAware{

    private String beanName;
    public void setBeanName(String name) {
        this.beanName=name;
    }
    private ResourceLoader loader;
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.loader=resourceLoader;
    }

    public void outputResult(){
        System.out.println("Bean的名字="+beanName);
        Resource resource=loader.getResource("com/envision/demo5/test.txt");
        try {
            byte[] b=new byte[1024];
            resource.getInputStream().read(b);
            System.out.println("resourceloader 加载的文件内容为"+ b.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
