package com.envision.demo3;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * <strong>Title : GreetingController.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年01月17日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@RestController
public class GreetingController {
    private static final String template="Helo ,%s!";
    private final AtomicLong counter=new AtomicLong();

    @RequestMapping(value = "/greeting")
    public Greeting greeting(@RequestParam(value = "name" ,defaultValue = "World") String name){
        return new Greeting(counter.incrementAndGet(),String.format(template,name));
    }


}
