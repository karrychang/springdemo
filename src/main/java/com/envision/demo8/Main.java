package com.envision.demo8;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <strong>Title : Main.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月12日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionConfig.class);
        ListService listService = context.getBean(ListService.class);
        String str=listService.showListCmd();
        System.out.println(str);
        context.close();


    }
}
