package com.envision.demo12;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <strong>Title : ReportExportHandler.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月14日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@RestController
@RequestMapping("/dat")
public class ReportExportHandler {

    /** 
    *************************************************************************
     * ReportExportHandler.java——exportpdf<br>
     * Author: 畅江涛<br>
     * Date: 2018/3/14<br>
     * Description:导出链接<br>
     * Used in：<br>
     * @param request
     * @param response
     * @return
     *************************************************************************
     */
    @RequestMapping("/exportpdf")
    public String exportpdf(HttpServletRequest request ,HttpServletResponse response) throws Exception {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        String filename="中山广场报告.pdf";
        String path="d:/";
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment;fileName="
                + URLEncoder.encode(filename, "UTF-8"));
        OutputStream os = null;
        PdfStamper ps = null;
        PdfReader reader = null;
        try {
            os = response.getOutputStream();
            // 2 读入pdf表单
            reader = new PdfReader(new URL(request.getRequestURL().toString().replace("dat/exportpdf","page/report.pdf")));
            // 3 根据表单生成一个新的pdf
            ps = new PdfStamper(reader, os);
            // 4 获取pdf表单
            AcroFields form = ps.getAcroFields();

            // 5给表单添加中文字体 这里采用系统字体。不设置的话，中文可能无法显示
//            BaseFont bf = BaseFont.createFont("C:/WINDOWS/Fonts/SIMSUN.TTC,1", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            form.addSubstitutionFont(bf);
            BaseFont bf = BaseFont.createFont("STSongStd-Light","UniGB-UCS2-H", false);
            form.addSubstitutionFont(bf);
            // 6查询数据================================================
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("name", "SOHO中山广场");
            data.put("projectname", "上海SOHO中山广场");
            data.put("address", "上海市长宁区中山西路1065号");
            data.put("cap", "800kWh");
            data.put("power", "400kW");
            data.put("power1", "400");
            data.put("num", "8");
            data.put("cap1", "test");
            // 7遍历data 给pdf表单表格赋值
            for (String key : data.keySet()) {
                form.setField(key,data.get(key).toString());
            }
            ps.setFormFlattening(true);
            //-----------------------------pdf 添加图片----------------------------------
            // 读图片
//            String url=request.getRequestURL().toString().replace(request.getRequestURI(),"")+"/envisiondat/dat";
            String url=request.getRequestURL().toString().replace(request.getRequestURI(),"")+"/dat";
            String[] strs={"imageline","imagebar","imageoneline"};
            int i=1;
            for (String string:strs){
                int pageNo = form.getFieldPositions("image"+i).get(0).page;
                Rectangle signRect = form.getFieldPositions("image"+i).get(0).position;
                float x = signRect.getLeft();
                float y = signRect.getBottom();
                // 获取操作的页面
                PdfContentByte under = ps.getOverContent(pageNo);
                // 根据域的大小缩放图片
//                PdfTemplate image = SVGConverter.test(signRect.getWidth(), signRect.getHeight(),url+"/"+string,under);

//                image.scaleToFit(signRect.getWidth(), signRect.getHeight());
//                // 添加图片
//                image.setAbsolutePosition(x, y);
//                under.add(image);

               i++;
            }
            //---------------------------add table ----------------------------------
            com.itextpdf.text.Font fontChinese = null;
            fontChinese = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);// 中文字体
            fontChinese.setSize(8);//字体大小
            PdfContentByte overContentByte = ps.getOverContent(8);
            PdfPTable pdfPTable = new PdfPTable(new float[]{90,90,90,90});
            pdfPTable.addCell(new Paragraph("内容", fontChinese));
            pdfPTable.addCell(new Paragraph("数值", fontChinese));
            pdfPTable.addCell(new Paragraph("单位", fontChinese));
            pdfPTable.addCell(new Paragraph("备注", fontChinese));
////            for (Somain s : list) {//将集合内的对象循环写入到表格
////                table.addCell(new Paragraph(s.getSoId(), fontChar));
////                table.addCell(new Paragraph(s.getCustomerCode(), fontChar));
////                table.addCell(new Paragraph(s.getName(), fontChar));
////                table.addCell(new Paragraph(s.getCreateTime(), fontChar));
////                table.addCell(new Paragraph(s.getAccount(), fontChar));
////                table.addCell(new Paragraph(String.valueOf(s.getSoTotal()), fontChar));
////                table.addCell(new Paragraph(String.valueOf(s.getPrePayFee()), fontChar));
////                if (Integer.parseInt(s.getPayType()) == 1) {
////                    table.addCell(new Paragraph("预付款发货", fontChinese));
////                } else if (Integer.parseInt(s.getPayType()) == 0) {
////                    table.addCell(new Paragraph("货到付款", fontChinese));
////                } else {
////                    table.addCell(new Paragraph("款到发货", fontChinese));
////                }
////            }
            pdfPTable.writeSelectedRows(0, -1, 110, 150, overContentByte);
            reader.close();

            //-------------------------------------------------------------
            System.out.println("===============PDF   success=============");
        } catch (Exception e) {
            System.out.println("===============PDF failed=============");
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                reader.close();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        File file = new File("itext.pdf");//文件名为itext.pdf， 默认路径下。
        Document document = new Document(PageSize.A4);
        BaseFont bf = null;
        Font fontChinese = null;
        Font fontChar = new Font();
        fontChar.setSize(7);
        fontChar.setColor(BaseColor.DARK_GRAY);
        try {
            bf = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
            fontChinese = new Font(bf, 10, Font.NORMAL);// 中文字体
            fontChinese.setSize(8);//字体大小
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));// 文档输出流。
            document.open();
            PdfPTable table = new PdfPTable(new float[]{28, 19, 15, 27, 12, 15, 14, 16});// 8列的表格以及单元格的宽度。

            table.setPaddingTop(2);// 顶部空白区高度
            table.setTotalWidth(360);//表格整体宽度
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Details of past one month on sale."));


            cell.setColspan(8);//占据八列
            cell.setRowspan(2);
            table.addCell(cell);
            table.addCell(new Paragraph("销售单号", fontChinese));
            table.addCell(new Paragraph("客户编号", fontChinese));
            table.addCell(new Paragraph("客户名称", fontChinese));
            table.addCell(new Paragraph("销售日期", fontChinese));
            table.addCell(new Paragraph("经手人", fontChinese));
            table.addCell(new Paragraph("总金额", fontChinese));
            table.addCell(new Paragraph("预付款", fontChinese));
            table.addCell(new Paragraph("购买方式", fontChinese));
//            for (Somain s : list) {//将集合内的对象循环写入到表格
//                table.addCell(new Paragraph(s.getSoId(), fontChar));
//                table.addCell(new Paragraph(s.getCustomerCode(), fontChar));
//                table.addCell(new Paragraph(s.getName(), fontChar));
//                table.addCell(new Paragraph(s.getCreateTime(), fontChar));
//                table.addCell(new Paragraph(s.getAccount(), fontChar));
//                table.addCell(new Paragraph(String.valueOf(s.getSoTotal()), fontChar));
//                table.addCell(new Paragraph(String.valueOf(s.getPrePayFee()), fontChar));
//                if (Integer.parseInt(s.getPayType()) == 1) {
//                    table.addCell(new Paragraph("预付款发货", fontChinese));
//                } else if (Integer.parseInt(s.getPayType()) == 0) {
//                    table.addCell(new Paragraph("货到付款", fontChinese));
//                } else {
//                    table.addCell(new Paragraph("款到发货", fontChinese));
//                }
//            }


            document.add(table);
            document.close();
            pdfWriter.flush();
            System.out.println("document itext pdf write finished...100%");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



//    public static void main(String[] args){
//        Document document = new Document();
//        try {
//            PdfWriter.getInstance(document,
//                    new FileOutputStream("TableCellBorder.pdf"));
//            document.open();
//
//            PdfPTable table = new PdfPTable(3);
//            PdfPCell cell1 = new PdfPCell(new Phrase("Cell 1"));
//            cell1.setUseBorderPadding(true);
//            //
//            // Setting cell's border width and color
//            //
//            cell1.setBorderWidth(5f);
//            cell1.setBorderColor(BaseColor.BLUE);
//            table.addCell(cell1);
//
//            PdfPCell cell2 = new PdfPCell(new Phrase("Cell 2"));
//            cell2.setUseBorderPadding(true);
//            //
//            // Setting cell's background color
//            //
//            cell2.setBackgroundColor(BaseColor.GRAY);
//            //
//            // Setting cell's individual border color
//            //
//            cell2.setBorderWidthTop(1f);
//            cell2.setBorderColorTop(BaseColor.RED);
//            cell2.setBorderColorRight(BaseColor.GREEN);
//            cell2.setBorderColorBottom(BaseColor.BLUE);
//            cell2.setBorderColorLeft(BaseColor.BLACK);
//            table.addCell(cell2);
//
//            PdfPCell cell3 = new PdfPCell(new Phrase("Cell 3"));
//            cell3.setUseBorderPadding(true);
//            //
//            // Setting cell's individual border width
//            //
//            cell3.setBorderWidthTop(2f);
//            cell3.setBorderWidthRight(1f);
//            cell3.setBorderWidthBottom(2f);
//            cell3.setBorderWidthLeft(1f);
//            table.addCell(cell3);
//            table.completeRow();
//
//            document.add(table);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            document.close();
//        }
//
//
//    }














}
