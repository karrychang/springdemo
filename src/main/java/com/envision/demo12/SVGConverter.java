package com.envision.demo12;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.io.*;
import java.util.Date;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.batik.bridge.*;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.transcoder.print.PrintTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.fop.svg.PDFTranscoder;
import org.w3c.dom.svg.SVGDocument;

/**
 * <strong>Title : SVGConverter.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月18日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class SVGConverter {
    public static void main(String[] args) throws Exception {
        Document document = new Document(PageSize.A4,36, 72, 108, 180);
        //构建svgDocument
        PdfWriter writer = PdfWriter.getInstance(document,
                new FileOutputStream("d:/test.pdf"));
        document.open();
        document.add(new Paragraph("SVG Example"));

        int width = 250;
        int height = 250;
        PdfContentByte cb = writer.getDirectContent();
        PdfTemplate template = cb.createTemplate(width,height);
        Graphics2D g2 = template.createGraphics(width,height);

        PrintTranscoder prm = new PrintTranscoder();
        TranscoderInput ti = new TranscoderInput("d:/gen.svg");//also tried E://firoz_trend.svg
        prm.transcode(ti, null);

        PageFormat pg = new PageFormat();
        Paper pp= new Paper();
        pp.setSize(width, height);
        pp.setImageableArea(0, 0, width, height);
        pg.setPaper(pp);
        prm.print(g2, pg, 0);
        g2.dispose();

        ImgTemplate img = new ImgTemplate(template);
        document.add(img);
    }
    public static PdfTemplate test(String url,PdfContentByte under)throws Exception{
        Document document = new Document(PageSize.A4,36, 72, 108, 180);
        //构建svgDocument
        PdfWriter writer = PdfWriter.getInstance(document,
                new FileOutputStream("d://test.pdf"));
        document.open();
        document.add(new Paragraph("SVG Example"));

        int width = 250;
        int height = 250;
        PdfContentByte cb = writer.getDirectContent();
        PdfTemplate template = cb.createTemplate(width,height);
        Graphics2D g2 = template.createGraphics(width,height);

        PrintTranscoder prm = new PrintTranscoder();
        TranscoderInput ti = new TranscoderInput("d://gen.svg");//also tried E://firoz_trend.svg
        prm.transcode(ti, null);

        PageFormat pg = new PageFormat();
        Paper pp= new Paper();
        pp.setSize(width, height);
        pp.setImageableArea(0, 0, width, height);
        pg.setPaper(pp);
        prm.print(g2, pg, 0);
        g2.dispose();

        ImgTemplate img = new ImgTemplate(template);
        document.add(img);

        return null;
    }

}
