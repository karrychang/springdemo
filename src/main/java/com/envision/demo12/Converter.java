package com.envision.demo12;


import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.*;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import org.apache.batik.bridge.*;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * <strong>Title : Converter.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月16日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class Converter {


    public static PdfTemplate svgToImgTemplate(float width, float height, String url, PdfContentByte pcb)throws  Exception{
//        //构建svgDocument
//        final String parser = XMLResourceDescriptor.getXMLParserClassName();
//
//        SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);
//        UserAgent userAgent = new UserAgentAdapter();
//        DocumentLoader loader = new DocumentLoader(userAgent);
//        BridgeContext ctx = new BridgeContext(userAgent, loader);
//        ctx.setDynamicState(BridgeContext.STATIC);
//        GVTBuilder builder = new GVTBuilder();
//
//        PdfTemplate template = pcb.createTemplate(width, height);
//        //生成awt Graphics2D
//        Graphics2D g2d = new PdfGraphics2D(template, width, height);
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        SVGDocument svgDocument = factory.createSVGDocument("http://m.amap.com/subway/#2102");
//        GraphicsNode graphNode = builder.build(ctx, svgDocument);
//        //画svg到画布
//        graphNode.paint(g2d);
//        g2d.dispose();
//        //生成Img
////        ImgTemplate img = new ImgTemplate(template);
//        return template;
        return null;
    }


    public static void main(String[] args) throws IOException {
//        File file = new File("TableCellBorder.pdf");//文件名为itext.pdf， 默认路径下。
//        Document document = new Document(PageSize.A4,36, 72, 108, 180);
//        BaseFont bf = null;
//        com.itextpdf.text.Font fontChinese = null;
//        com.itextpdf.text.Font fontChar = new com.itextpdf.text.Font();
//        fontChar.setSize(7);
//        fontChar.setColor(BaseColor.DARK_GRAY);
//        try {
//            bf = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
//            fontChinese = new com.itextpdf.text.Font(bf, 10, com.itextpdf.text.Font.NORMAL);// 中文字体
//            fontChinese.setSize(8);//字体大小
//            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));// 文档输出流。
//            document.open();
//
//            PdfPTable table = new PdfPTable(new float[]{28, 19, 15, 27, 12, 15, 14, 16});// 8列的表格以及单元格的宽度。
//
//            table.setPaddingTop(20);// 顶部空白区高度
//            table.setTotalWidth(360);//表格整体宽度
//            PdfPCell cell;
//            cell = new PdfPCell(new Phrase("Details of past one month on sale."));
//
//
//            cell.setColspan(8);//占据八列
//            cell.setRowspan(2);
//            table.addCell(cell);
//            table.addCell(new Paragraph("销售单号", fontChinese));
//            table.addCell(new Paragraph("客户编号", fontChinese));
//            table.addCell(new Paragraph("客户名称", fontChinese));
//            table.addCell(new Paragraph("销售日期", fontChinese));
//            table.addCell(new Paragraph("经手人", fontChinese));
//            table.addCell(new Paragraph("总金额", fontChinese));
//            table.addCell(new Paragraph("预付款", fontChinese));
//            table.addCell(new Paragraph("购买方式", fontChinese));
////            for (Somain s : list) {//将集合内的对象循环写入到表格
////                table.addCell(new Paragraph(s.getSoId(), fontChar));
////                table.addCell(new Paragraph(s.getCustomerCode(), fontChar));
////                table.addCell(new Paragraph(s.getName(), fontChar));
////                table.addCell(new Paragraph(s.getCreateTime(), fontChar));
////                table.addCell(new Paragraph(s.getAccount(), fontChar));
////                table.addCell(new Paragraph(String.valueOf(s.getSoTotal()), fontChar));
////                table.addCell(new Paragraph(String.valueOf(s.getPrePayFee()), fontChar));
////                if (Integer.parseInt(s.getPayType()) == 1) {
////                    table.addCell(new Paragraph("预付款发货", fontChinese));
////                } else if (Integer.parseInt(s.getPayType()) == 0) {
////                    table.addCell(new Paragraph("货到付款", fontChinese));
////                } else {
////                    table.addCell(new Paragraph("款到发货", fontChinese));
////                }
////            }
//            table.writeSelectedRows(0,-1,1,2,pdfWriter.getDirectContent());
//            document.add(table);
//            document.close();
//            pdfWriter.flush();
//            System.out.println("document itext pdf write finished...100%");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        PdfReader reader = new PdfReader("report.pdf");
//        FileOutputStream fileOutputStream = new FileOutputStream("test.pdf");
//        try {
//            BaseFont bf = BaseFont.createFont("STSongStd-Light","UniGB-UCS2-H", false);
//            PdfStamper stamper= new PdfStamper(reader, fileOutputStream);
//
//
//
////            com.itextpdf.text.Font fontChinese = null;
//            com.itextpdf.text.Font fontChinese = new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.NORMAL);// 中文字体
//            PdfContentByte overContentByte = stamper.getOverContent(8);
//            PdfPTable pdfPTable = new PdfPTable(new float[]{150,58,45,128});
//            pdfPTable.setTotalWidth(381);
//            String [] strs ={"内容","数值","单位","备注"};
//            for (String string:strs){
//                PdfPCell cell=new PdfPCell(new Paragraph(string, new com.itextpdf.text.Font(bf, 12, com.itextpdf.text.Font.BOLD)));
//                cell.setFixedHeight(22);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cell.setBackgroundColor(new BaseColor(217,217,217));
//                pdfPTable.addCell(cell);
//            }
//            pdfPTable.setHorizontalAlignment(Element.ALIGN_LEFT);
//            for (int i=0;i<19;i++) {
//                for (int j=0;j<4;j++){
//                    PdfPCell cell=new PdfPCell(new Paragraph("test"+i, fontChinese));
//                    cell.setFixedHeight(20);
//                    if (j==1){
//                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                    }else {
//                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                    }
//                    pdfPTable.addCell(cell);
//                }
//
//            }
//            pdfPTable.writeSelectedRows(0, -1, 109, 700, overContentByte);
//
//
//
//
//            stamper.close();
//            reader.close();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
    }



//        public static void main(String[] args) {
//            Document document = new Document();
//            try {
//                PdfWriter.getInstance(document, new FileOutputStream("TableCellBorder.pdf"));
//                document.open();
//
//                PdfPTable table = new PdfPTable(3);
//                PdfPCell cell1 = new PdfPCell(new Phrase("Cell 1"));
//                cell1.setUseBorderPadding(true);
//                //
//                // Setting cell's border width and color
//                //
//                cell1.setBorderWidth(5f);
//                cell1.setBorderColor(BaseColor.BLUE);
//                table.addCell(cell1);
//
//                PdfPCell cell2 = new PdfPCell(new Phrase("Cell 2"));
//                cell2.setUseBorderPadding(true);
//                //
//                // Setting cell's background color
//                //
//                cell2.setBackgroundColor(BaseColor.GRAY);
//                //
//                // Setting cell's individual border color
//                //
//                cell2.setBorderWidthTop(1f);
//                cell2.setBorderColorTop(BaseColor.RED);
//                cell2.setBorderColorRight(BaseColor.GREEN);
//                cell2.setBorderColorBottom(BaseColor.BLUE);
//                cell2.setBorderColorLeft(BaseColor.BLACK);
//                table.addCell(cell2);
//
//                PdfPCell cell3 = new PdfPCell(new Phrase("Cell 3"));
//                cell3.setUseBorderPadding(true);
//                //
//                // Setting cell's individual border width
//                //
//                cell3.setBorderWidthTop(2f);
//                cell3.setBorderWidthRight(1f);
//                cell3.setBorderWidthBottom(2f);
//                cell3.setBorderWidthLeft(1f);
//                table.addCell(cell3);
//                table.completeRow();
//
//                document.add(table);
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                document.close();
//            }

//        }


}
