package com.envision.demo12;


import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 * <strong>Title : JFreeChartUtils.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月14日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class JFreeChartUtils {

    public static CategoryDataset getDataSetBar() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<24;i++){
            if(i==23){
                dataset.addValue(Math.random(), "夏季", "23~0");
                dataset.addValue(Math.random(), "非夏季", "23~0");
                break;
            }
            dataset.addValue(Math.random(), "夏季", i+"~"+(i+1));
            dataset.addValue(Math.random(), "非夏季", i+"~"+(i+1));
        }
        return dataset;
    }

    public static CategoryDataset createDataLine() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=1;i<11;i++){
            dataset.addValue(Math.random()*1000000, "方案一", "第"+i+"年");
            dataset.addValue(Math.random()*1000000, "方案二", "第"+i+"年");
            dataset.addValue(Math.random()*1000000, "方案三", "第"+i+"年");
        }
        return dataset;
    }

    public static CategoryDataset createDatasetOneLine() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<24;i++){
            if(i==23){
                dataset.addValue(Math.random()*50000, "负荷分析", "23~0");
                break;
            }
            dataset.addValue(Math.random()*50000, "负荷分析", i+"~"+(i+1));
        }
        return dataset;
    }

}
