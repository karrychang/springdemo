package com.envision.demo12;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.ui.RectangleInsets;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.*;

/**
 * <strong>Title : JFreeChartHandler.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年03月14日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@RestController
@RequestMapping("/dat")
public class JFreeChartHandler {

    @RequestMapping("/imagebar")
    public void createimage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CategoryDataset dataset =JFreeChartUtils.getDataSetBar();
        chartCommomConfig();

        JFreeChart chart = ChartFactory.createBarChart("电价分布", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );
        jfreechartconfig(chart);
        CategoryPlot categoryPlot=chart.getCategoryPlot();

        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);//横坐标偏移
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 19));

        ValueAxis rAxis =categoryPlot.getRangeAxis();//对Y轴做操作
        NumberAxis numberAxis = (NumberAxis)rAxis;
        numberAxis .setAutoTickUnitSelection(false);
        double unit=0.2;//刻度的长度
        NumberTickUnit ntu= new NumberTickUnit(unit);
        numberAxis .setTickUnit(ntu);

        BarRenderer renderer=new BarRenderer();
        renderer.setItemMargin(0.001);//设置每个地区所包含的平行柱的之间距离
        renderer.setDrawBarOutline(false);//边框
        renderer.setShadowVisible(false);//阴影
        renderer.setSeriesPaint(0, new Color(68, 92, 187));//颜色设置
        renderer.setSeriesPaint(1, new Color(187, 68, 68));
        renderer.setBarPainter(new StandardBarPainter());//取消柱子渐变效果
        renderer.setMaximumBarWidth(0.01);//柱子宽度百分比
        categoryPlot.setRenderer(renderer);

//        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 880, 600,
//                null);
//        ChartUtilities.writeChartAsPNG(response.getOutputStream(), chart, 900, 520, true,
        response.addHeader("Content-Type", "image/svg+xml");
        JFreeChartHandler.exportChartAsSVG(chart,900,520,response);

    }



    /** 
    *************************************************************************
     * JFreeChartHandler.java——createarea1<br>
     * Author: 畅江涛<br>
     * Date: 2018/3/14<br>
     * Description:收益总览<br>
     * Used in：<br>
     * @param response
     * @return
     *************************************************************************
     */
    @RequestMapping("/imageline")
    public void createarea1(HttpServletResponse response) throws IOException {
        CategoryDataset dataset = JFreeChartUtils.createDataLine();
        chartCommomConfig();
        JFreeChart chart =ChartFactory.createLineChart("收益总览单位：元", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );

        jfreechartconfig(chart);
        CategoryPlot categoryPlot=chart.getCategoryPlot();
        LineAndShapeRenderer renderer=(LineAndShapeRenderer)categoryPlot.getRenderer();
        renderer.setSeriesStroke(0, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(0, new Color(34,146,211));//红色
        renderer.setSeriesStroke(1, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(1, new Color(187,68,68));//红色
        renderer.setSeriesStroke(2, new BasicStroke(2.0F));//设置折线大小
        renderer.setSeriesPaint(2,new Color(138,179,77));//红色

        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setTickLabelFont(getFont(Font.PLAIN,19f));




//        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 600, 400,
//                null);
        response.addHeader("Content-Type", "image/svg+xml");
        JFreeChartHandler.exportChartAsSVG(chart,900,520,response);
    }
    
    /** 
    *************************************************************************
     * JFreeChartHandler.java——createarea2<br>
     * Author: 畅江涛<br>
     * Date: 2018/3/14<br>
     * Description:负荷分析<br>
     * Used in：<br>
     * @param response
     * @return
     *************************************************************************
     */
    @RequestMapping("/imageoneline")
    public void createarea2(HttpServletResponse response) throws IOException {
        CategoryDataset dataset = JFreeChartUtils.createDatasetOneLine();
        chartCommomConfig();
        JFreeChart chart =ChartFactory.createLineChart("负荷分析", // 图表标题
                null, // 目录轴的显示标签
                null, // 数值轴的显示标签
                dataset, // 数据集
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直
                true, // 是否显示图例(对于简单的柱状图必须是 false)
                false, // 是否生成工具
                false // 是否生成 URL 链接
        );

        jfreechartconfig(chart);
        CategoryPlot categoryPlot=chart.getCategoryPlot();
        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);//横坐标偏移
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 19));


        LineAndShapeRenderer renderer=(LineAndShapeRenderer)categoryPlot.getRenderer();
        renderer.setBaseShapesVisible(true);
        renderer.setBaseShapesFilled(true);
        renderer.setBaseFillPaint(Color.blue);
        renderer.setSeriesStroke(0, new BasicStroke(3.0F));//设置折线大小
        renderer.setSeriesPaint(0, Color.blue);
        renderer.setSeriesShape(0, new Ellipse2D.Double(-2D, -2D, 4D, 4D));
        
        ChartUtilities.writeChartAsJPEG(response.getOutputStream(), 1, chart, 880, 600, null);
        response.addHeader("Content-Type", "image/svg+xml");
        JFreeChartHandler.exportChartAsSVG(chart,900,520,response);
    }

    /** 
    *************************************************************************
     * JFreeChartHandler.java——chartCommomConfig<br>
     * Author: 畅江涛<br>
     * Date: 2018/3/14<br>
     * Description:中文乱码问题<br>
     * Used in：<br>
     * @param 
     * @return
     *************************************************************************
     */
    public void chartCommomConfig(){

        //创建主题样式
        StandardChartTheme standardChartTheme=new StandardChartTheme("CN");
        //设置标题字体
//        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN,14));
        standardChartTheme.setExtraLargeFont(getFont(Font.PLAIN,25f));
        //设置图例的字体
        standardChartTheme.setRegularFont(getFont(Font.PLAIN,19f));
        //设置轴向的字体
        standardChartTheme.setLargeFont(getFont(Font.PLAIN,19f));
        //应用主题样式
        ChartFactory.setChartTheme(standardChartTheme);
    }
    /** 
    *************************************************************************
     * JFreeChartHandler.java——jfreechartconfig<br>
     * Author: 畅江涛<br>
     * Date: 2018/3/14<br>
     * Description:公共配置<br>
     * Used in：<br>
     * @param chart
     * @return
     *************************************************************************
     */
    public void jfreechartconfig(JFreeChart chart){

//        Font font=new Font("宋体",Font.PLAIN,9);
//        Font fontTitle=new Font("宋体", Font.PLAIN,14);

        chart.setBorderVisible(false);//边界不可见
        chart.getLegend().setBorder(0,0,0,0);//图示边框
//        chart.getLegend().setItemFont(font);
//        chart.getTitle().setFont(fontTitle);

        Plot plot=chart.getPlot();
        plot.setBackgroundPaint(ChartColor.white);//背景白色
        plot.setOutlineVisible(false);

        CategoryPlot categoryPlot=chart.getCategoryPlot();
        categoryPlot.setRangeGridlinePaint(Color.lightGray);//横向线颜色
        categoryPlot.setRangeGridlinesVisible(true);//是否可见
        categoryPlot.setRangeGridlineStroke(new BasicStroke(0.5f));//横向背景线为实线
        categoryPlot.setAxisOffset(new RectangleInsets(1D, 0D, 0D, 2D));



        CategoryAxis domainAxis=categoryPlot.getDomainAxis();//对X轴做操作
//        domainAxis.setTickLabelFont(getFont(Font.PLAIN,9f));
        domainAxis.setLabelFont(getFont(Font.PLAIN,14f));//设置X轴的标题文字
        domainAxis.setTickMarksVisible(false);//标记线是否显示
        domainAxis.setUpperMargin(0.005);//设置距离图片左端距离
        domainAxis.setLowerMargin(0.005); //设置距离图片右端距离

        ValueAxis rAxis =categoryPlot.getRangeAxis();//对Y轴做操作
        //设置Y轴坐标上的文字
        rAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 19));
        rAxis.setTickMarkPaint(Color.white);//刻度线颜色
        rAxis.setTickLabelsVisible(true);//刻度数值是否显示
        rAxis.setTickMarksVisible(true);
        rAxis.setAxisLineVisible(false);//Y轴竖线是否显示
//        rAxis.setLabelFont(font);

    }



    private static File file = null;
    private static void initFontFile() {
        if (file == null) {
            String vPath = JFreeChartUtils.class.getClassLoader().getResource("/stsong.ttf").getPath();
//            String vPath = this.getServletContext().getRealPath("/WEB-INF/classes/simsunb.ttf");
            System.out.println(vPath);
            file = new File(vPath);
        }
    }
    private static Font getFont(int style, Float size) {
        Font defFont = new Font("黑体", style, 19);
        try {
            initFontFile();
            if (file == null || !file.exists()) {
                System.out.println("file == null || !file.exists()");
                return defFont;
            }
            java.io.FileInputStream fi = new java.io.FileInputStream(file);
            Font nf = Font.createFont(Font.TRUETYPE_FONT, fi);
            fi.close();
            // 这一句需要注意
            // Font.deriveFont() 方法用来创建一个新的字体对象
            nf = nf.deriveFont(style, size);
            System.out.println(nf.toString());
            return nf;
        } catch (Exception e) {

        }
        return defFont;
    }

    private static void exportChartAsSVG(JFreeChart chart, int width,int height, HttpServletResponse response) throws IOException {
        // Get a DOMImplementation and create an XML document
        DOMImplementation domImpl =
                GenericDOMImplementation.getDOMImplementation();
        Document document = domImpl.createDocument(null, "svg", null);

        // Create an instance of the SVG Generator
        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
        Rectangle bounds = new Rectangle(0,0,width,height);
        // draw the chart in the SVG generator
        chart.draw(svgGenerator, bounds);

        // Write svg file
        OutputStream outputStream = response.getOutputStream();
        Writer out = new OutputStreamWriter(outputStream, "UTF-8");
        svgGenerator.stream(out, true);
        outputStream.flush();
        outputStream.close();
    }
}
