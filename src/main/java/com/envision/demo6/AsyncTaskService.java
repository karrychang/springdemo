package com.envision.demo6;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * <strong>Title : AsyncTaskService.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月12日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Service
public class AsyncTaskService {
    @Async
    public void executeAsynTask(Integer i){
        System.out.println("执行task异步任务"+i);
    }
    @Async
    public void executeAsynTaskPlus(Integer i){
        System.out.println("执行task异步任务+1="+i);
    }


}
