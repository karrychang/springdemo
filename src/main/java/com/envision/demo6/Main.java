package com.envision.demo6;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <strong>Title : Main.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月12日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TaskExecutorConfig.class);
        AsyncTaskService ats = context.getBean(AsyncTaskService.class);
        Long before=System.currentTimeMillis();
        System.out.println(before);
        for (int i =0;i<10;i++){
            ats.executeAsynTask(i);
            ats.executeAsynTaskPlus(i);
        }
        Long after = System.currentTimeMillis();
        System.out.println(after);
        System.out.println(after-before);
        context.close();
    }

}
