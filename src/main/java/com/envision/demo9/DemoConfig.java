package com.envision.demo9;

import org.springframework.stereotype.Service;

/**
 * <strong>Title : DemoConfig.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月12日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@testAnnotation("com.envision.demo9")
public class DemoConfig {
}
