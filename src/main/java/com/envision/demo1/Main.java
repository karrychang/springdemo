package com.envision.demo1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * <strong>Title : Main.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年01月17日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(ScopeConfig.class);
        DemoSingletonService dss1=context.getBean(DemoSingletonService.class);
        DemoSingletonService dss2=context.getBean(DemoSingletonService.class);
        System.out.println(dss1==dss2);
        DemoPrototypeService dps1=context.getBean(DemoPrototypeService.class);
        DemoPrototypeService dps2=context.getBean(DemoPrototypeService.class);
        System.out.println(dps1==dps2);
    }
}
