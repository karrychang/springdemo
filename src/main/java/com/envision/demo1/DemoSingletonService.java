package com.envision.demo1;

import org.springframework.stereotype.Service;

/**
 * <strong>Title : DemoSingletonService.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年01月17日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Service
public class DemoSingletonService {
}
