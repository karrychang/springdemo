package com.envision.demo7;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <strong>Title : ScheduledTaskService.java<br>
 * </strong> <strong>Description : </strong><br>
 * <strong>Create on : 2018年02月12日<br>
 * <p>
 *
 * @author 畅江涛<br>
 * @version <strong>1.0.0</strong><br>
 * <br>
 * <strong>修改历史:</strong><br>
 * 修改人		修改日期		修改描述<br>
 * -------------------------------------------<br>
 * <br>
 * <br>
 */
@Service
public class ScheduledTaskService {
    private static final SimpleDateFormat dateFormat=new SimpleDateFormat("HH:mm:ss");
    @Scheduled(fixedRate = 5000)
    public void task1(){
        System.out.println("每隔5秒执行执行一次"+dateFormat.format(new Date()));
    }
    @Scheduled(cron = "0 45 16 ? * *")
    public void task2(){
        System.out.println("在指定时间执行"+dateFormat.format(new Date()));
    }
}
